import 'dart:ui';

class Colorthemes {
  static const List<Color> foreground = [Color(0xFFFEFAE0)];
  static const List<Color> background = [Color(0xFF283618)];
  static const List<Color> backgroundlight = [Color(0xFF606C38)];
  static const List<Color> accent = [Color(0xFFBC6C25)];
  static const List<Color> accentlight = [Color(0xFFDDA15E)];
}
