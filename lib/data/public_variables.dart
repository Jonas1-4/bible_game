int theme = 0;

dynamic bible = {};

//SharedPrefs const

//saving them as const here to avoid misspelling or 
//dublicate errors

const String spLanguage                 = 'language';
const String spLanguageIndex            = 'languageIndex';
const String spBibleVersionIndex        = 'bibleVersionIndex';
const String spBibleVersionName         = 'bibleVersionName';
const String spBibleVersionJson         = 'bibleVersionJson';
const String spVerseLevel               = 'verseLevel';
const String spChapterLevel             = 'chapterLevel';  
const String spSelectedVerse            = 'selectedVerse';
